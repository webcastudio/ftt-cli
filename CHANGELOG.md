## 0.0.1 (2019-03-27)


### Bug Fixes

* cannot get cuepoints ([881b1f9](https://gitlab.com/webcastudio/ftt-cli/commit/881b1f9)), closes [#1](https://gitlab.com/webcastudio/ftt-cli/issues/1)


### Features

* find cues ([9cae72e](https://gitlab.com/webcastudio/ftt-cli/commit/9cae72e))
* get cues file ([ae09287](https://gitlab.com/webcastudio/ftt-cli/commit/ae09287))
* head and tail commands ([c626726](https://gitlab.com/webcastudio/ftt-cli/commit/c626726))
* upload cues ([8229b7b](https://gitlab.com/webcastudio/ftt-cli/commit/8229b7b))


