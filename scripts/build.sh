#!/usr/bin/env bash

rm -rf build

./node_modules/.bin/tsc -p tsconfig.app.json

chmod +x build/src/index.js

cp README.md build
cp LICENSE build

# Prepare package.json for release
./scripts/package.js
