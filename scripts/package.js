#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

const PKG_PATH = path.resolve(path.join(__dirname, '../build/package.json'));

const pkg = require(PKG_PATH);

// Remove devDependencies and scripts
delete pkg.devDependencies;
delete pkg.scripts;


fs.writeFileSync(PKG_PATH, JSON.stringify(pkg, null, 2));
