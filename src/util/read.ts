import {readFileSync} from 'fs';
import {Cue} from '../model/cue';

export function read(file: string): Cue[] {
  let data;

  data = readFileSync(file);
  data = JSON.parse(data);

  if (data.cuepoints) {
    data = data.cuepoints;
  }

  if (!Array.isArray(data)) {
    throw new Error('Invalid cues file');
  }
  return data;
}
