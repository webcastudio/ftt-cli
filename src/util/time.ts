/*export function time2str(time: number): string {
  if (time === null || time === undefined) {
    return null;
  }
  const hours = Math.floor(time / 3600);
  const minutes = Math.floor( time / 60 ) % 60;
  const seconds = time % 60;
  return [hours, minutes, seconds]
    .map(v => v < 10 ? '0' + v : v)
    .join(':');
}*/

export function str2time(time: string): number {
  const dur = time.split(':');
  const hour = parseInt(dur[0], 10) * 3600;
  const minutes = parseInt(dur[1], 10) * 60;
  const seconds = parseInt(dur[2], 10);

  return hour + minutes + seconds;
}
