import {table} from 'table';

export function print(cues: any[], components: string[] = null) {
  if (components && components.length === 0) {
    components = null;
  }
  const componentColumnWidth = 10;
  const componentsMap = {};
  const header: string[] = ['start', 'end'];

  const rows: any[] = [header];

  const config = {
    columns: {}
  };

  if (!components || components.indexOf('container') >= 0) {
    config.columns[header.length] = {
      width: componentColumnWidth,
      wrapWord: true
    };
    componentsMap['container'] = header.length;
    header.push('container');
  }

  cues.forEach(c => {
    const cue = [c.startTime, c.endTime];
    Object.keys(componentsMap).forEach(() => {
      cue.push('');
    });
    c.actions.forEach(a => {
      let payload = '';

      if (components && components.indexOf(a.component) === -1) {
        return;
      }

      if (!componentsMap[a.component]) {
        componentsMap[a.component] = header.length;
        config.columns[header.length] = {
          width: componentColumnWidth,
          wrapWord: true
        };
        header.push(a.component);
      }

      try {
        if (a.action === 'default') {
          payload = 'a: D';
        } else {
          payload = `a: ${a.action.substr(0, 1).toUpperCase()}\n`;
          if (a.payload.layoutClass) {
            payload += `l: ${a.payload.layoutClass}`;
          } else if (a.payload.slide) {
            payload += `cId: ${a.payload.contentId}\n` +
              `slide: ${a.payload.slide}\n` +
              `step: ${a.payload.step}`;
          } else {
            payload += `cId: ${a.payload.contentId}`;
          }
        }
      } catch (e) {
        payload = 'parse error';
      }

      cue[componentsMap[a.component]] = payload;
    });
    rows.push(cue);
  });

  rows.forEach(cue => {
    if (cue.length < header.length) {
      for (let i = 0; i < (header.length - cue.length); i++) {
        cue.push('');
      }
    }
  });

  console.log(table(rows, config));
}
