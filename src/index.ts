#!/usr/bin/env node

import pkg from '../package.json';
import {Command} from 'commander';

import {head, tail} from './parse.actions';
import {list} from './util/formats';
import {find} from './find.actions';
import {get} from './get.actions';
import {put} from './put.actions';

const program = new Command();

program
  .version(pkg.version);

const parseCommands = {
  'tail': {
    description: 'Shows N first cues.',
    fn: tail
  },
  'head': {
    description: 'Shows N last cues.',
    fn: head
  }
};

Object.keys(parseCommands).forEach(c => {
  const cmd = parseCommands[c];
  program
    .command(`${c} <file>`)
    .description(cmd.description)
    .option('-n, --number [rows]', 'Number of rows to show', 50)
    .option('-c, --components [components]', 'Component to show. List separated by comma i.e. container,headers.', list)
    .action(cmd.fn);
});

program
  .command('find <file>')
  .description('Find cues')
  .option('-t, --time [time]', 'Find cues at specific time')
  .option('-c, --components [components]', 'Component to show. List separated by comma i.e. container,headers.', list)
  .action(find);

program
  .command('get')
  .description('Download cues file')
  .option('-u, --url [url]', 'Customer api url', 'https://api.vancastvideo.com/api')
  .option('-k, --key <token>', 'Workspace Api Key token')
  .option('-e, --eventId <id>', 'Webcast Event id', parseInt)
  .option('-s, --sessionId <id>', 'Webcast Session id', parseInt)
  .option('-l, --languageId [id]', 'Webcast Language id', parseInt)
  .option('-o, --output <file>', 'Output file path')
  .option('-f, --force', 'Override file if exists')
  .action(cmd => {
    get(cmd)
      .then(() => {
        console.log('Cues file downloaded successfully!');
      })
      .catch(e => {
        console.error(e.message);
        process.exit(1);
      });
  });

program
  .command('put <file>')
  .description('Saves a cuepoint file to the specific OnDemand')
  .option('-u, --url [url]', 'Customer api url', 'https://api.vancastvideo.com/api')
  .option('-k, --key <token>', 'Workspace Api Key token')
  .option('-e, --eventId <id>', 'Webcast Event id', parseInt)
  .option('-s, --sessionId <id>', 'Webcast Session id', parseInt)
  .option('-l, --languageId <id>', 'Webcast Language id', parseInt)
  .action((file, cmd) => {
    put(file, cmd)
      .then(() => {
        console.log('Cues file uploaded successfully!');
      })
      .catch(e => {
        console.error(e.message);
        process.exit(1);
      });
  });

program.parse(process.argv);
