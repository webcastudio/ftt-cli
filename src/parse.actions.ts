import {print} from './util/print';
import {read} from './util/read';

export interface ParseActionOptions {
  components?: string[];
  number?: number;
}

export function tail(file: string, cmd: ParseActionOptions) {
  let data;

  try {
    data = read(file);
  } catch (e) {
    console.error('The file provided is not a valid JSON file.');
    process.exit(1);
    return;
  }

  const rows = data.length < cmd.number ? data : data.slice(-cmd.number);

  print(rows, cmd.components);
}

export function head(file: string, cmd: ParseActionOptions) {
  let data;

  try {
    data = read(file);
  } catch (e) {
    console.error('The file provided is not a valid JSON file.');
    process.exit(1);
    return;
  }

  const rows = data.length < cmd.number ? data : data.slice(0, cmd.number);

  print(rows, cmd.components);

}
