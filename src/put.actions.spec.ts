import {Cue} from './model/cue';
import {put} from './put.actions';

jest.mock('fs');
const {readFileSync} = require('fs');

jest.mock('@webcastudio/sdk');

const {WebcastEventsApi} = require('@webcastudio/sdk');

const mockUploadCues = jest.fn();
const mockFindOnDemands = jest.fn();
WebcastEventsApi.mockImplementation(() => {
  return {
    findOnDemands: mockFindOnDemands,
    uploadOndemandCuepoints: mockUploadCues
  };
});


describe('put actions', () => {
  beforeEach(() => {
    readFileSync.mockClear();
    mockUploadCues.mockClear();
  });

  describe('put', () => {
    it('should upload cuepoints file', async () => {
      const cues: Cue[] = [{
        version: '2',
        startTime: 0,
        endTime: null,
        actions: [{
          component: 'container',
          action: 'default'
        }]
      }];
      const cuesBuffer: Buffer = Buffer.from(JSON.stringify(cues));
      const od: any = {
        id: 1,
        eventId: 1,
        sessionId: 1,
        languageId: 1,
        cuepoints: [],
        media: {
          id: 1
        }
      };

      mockFindOnDemands.mockReturnValue(Promise.resolve({
        items: [od],
        totalItems: 1,
        isTruncated: false
      }));

      mockUploadCues.mockReturnValue(Promise.resolve({
       ...od,
       cuepoints: cues
      }));

      readFileSync.mockReturnValue(cuesBuffer);

      await put('cues.json', {
        url: 'api.url',
        key: 'token',
        eventId: 1,
        sessionId: 1,
        languageId: 1
      });

      expect(mockUploadCues).toHaveBeenCalledWith(1, 1, cuesBuffer);
    });
  });
});
