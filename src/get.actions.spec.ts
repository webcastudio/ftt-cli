import {get} from './get.actions';

jest.mock('fs');
const {existsSync, writeFileSync} = require('fs');

jest.mock('@webcastudio/sdk');

const {WebcastEventsApi} = require('@webcastudio/sdk');

const mockFindOnDemands = jest.fn();
WebcastEventsApi.mockImplementation(() => {
  return {findOnDemands: mockFindOnDemands};
});

describe('get actions', () => {
  describe('get', () => {
    beforeEach(() => {
      mockFindOnDemands.mockClear();
      existsSync.mockClear();
      writeFileSync.mockClear();
    });

    it('should get cuepoints from ws api and write them into the output file', async () => {
      existsSync
        .mockReturnValue(false);
      writeFileSync
        .mockReturnValue(true);

      const ods: any[] = [
        {
          id: 1,
          eventId: 1,
          sessionId: 1,
          languageId: 1,
          cuepoints: [{
            version: 2,
            startTime: 0,
            endTime: null,
            actions: [{
              component: 'container',
              action: 'default'
            }]
          }]
        }];

      mockFindOnDemands.mockReturnValue(Promise.resolve({
        items: ods,
        totalItems: ods.length,
        isTruncated: false
      }));

      await get({
        url: 'https://localhost:3000/api',
        key: 'token',
        eventId: 1,
        sessionId: 1,
        output: 'file.json'
      });

      expect(JSON.parse(writeFileSync.mock.calls[0][1])).toEqual(ods[0].cuepoints);
    });

    it('should not write the file if it already exists and force flag is not enabled', async () => {
      existsSync
        .mockReturnValue(true);

      const ods: any[] = [
        {
          id: 1,
          eventId: 1,
          sessionId: 1,
          languageId: 1,
          cuepoints: [{
            version: 2,
            startTime: 0,
            endTime: null,
            actions: [{
              component: 'container',
              action: 'default'
            }]
          }]
        }];

      mockFindOnDemands.mockReturnValue(Promise.resolve({
        items: ods,
        totalItems: ods.length,
        isTruncated: false
      }));

      try {
        await get({
          url: 'https://localhost:3000/api',
          key: 'token',
          eventId: 1,
          sessionId: 1,
          output: 'file.json'
        });
      } catch (e) {
        expect(e.message).toMatch(/Output file already exists/);
      }
      expect(writeFileSync).not.toHaveBeenCalled();
    });
  });
});
