import {ApiClient, WebcastEventsApi} from '@webcastudio/sdk';
import {existsSync, writeFileSync} from 'fs';
import {ApiActionOptionsBase} from './model/api-action-options-base';

export interface GetCuesOptions extends ApiActionOptionsBase {
  eventId: number;
  sessionId: number;
  languageId?: number;
  force?: boolean;
  output: string;
}

export async function get(cmd: GetCuesOptions) {
  ApiClient.configure({
    url: cmd.url,
    credentials: {
      apiKey: cmd.key
    }
  });
  const events = new WebcastEventsApi();

  const where: any = {
    sessionId: cmd.sessionId
  };

  if (cmd.languageId) {
    where.languageId = cmd.languageId;
  }

  const res = await events.findOnDemands(cmd.eventId, {
    where: JSON.stringify(where)
  });
  if (res.totalItems === 0) {
    throw new Error('No OnDemands has been created for this session.');
  }

  if (existsSync(cmd.output) && !cmd.force) {
    throw new Error('Output file already exists. Use option --force to force override.');
  }

  writeFileSync(cmd.output, JSON.stringify(res.items[0].cuepoints, null, 2), {encoding: 'utf-8'});
}
