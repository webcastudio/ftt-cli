import {find} from './find.actions';

jest.mock('fs');
const {readFileSync} = require('fs');

describe('parse actions', () => {
  let cuepoints;
  let stdout;


  beforeEach(() => {
    cuepoints = [
      {
        version: 2,
        startTime: 0,
        endTime: 10,
        actions: [
          {
            component: 'container',
            action: 'default'
          },
          {
            component: 'slideshows',
            action: 'default'
          }
        ]
      },
      {
        version: 2,
        startTime: 10,
        endTime: 20,
        actions: [
          {
            component: 'container',
            action: 'changeLayout',
            payload: {
              layoutClass: 'layout2'
            }
          },
          {
            component: 'slideshows',
            action: 'default'
          }
        ]
      },
      {
        version: 2,
        startTime: 20,
        endTime: 30,
        actions: [
          {
            component: 'container',
            action: 'refreshLayout',
            payload: {
              layoutClass: 'layout2'
            }
          },
          {
            component: 'slideshows',
            action: 'changeSlide',
            payload: {
              contentId: 1,
              slide: 1,
              step: 0
            }
          }
        ]
      },
      {
        version: 2,
        startTime: 30,
        endTime: 40,
        actions: [
          {
            component: 'container',
            action: 'refreshLayout',
            payload: {
              layoutClass: 'layout2'
            }
          },
          {
            component: 'slideshows',
            action: 'refreshSlide',
            payload: {
              contentId: 1,
              slide: 1,
              step: 0
            }
          }
        ]
      }
    ];
    readFileSync
      .mockReturnValue(Buffer.from(JSON.stringify(cuepoints)));
  });

  beforeEach(() => {
    stdout = jest.spyOn(console, 'log');
  });

  afterEach(() => {
    stdout.mockRestore();
  });

  it('should find and print cue given a certain time', () => {
    find('cuepoints.json', {
      time: 15
    });

    expect(stdout.mock.calls[0][0]).toMatch(/a: C/);
    expect(stdout.mock.calls[0][0]).toMatch(/l: layout2/);
  });

  it('should parse duration strings to seconds time', () => {
    find('cuepoints.json', {
      time: '00:00:15'
    });

    expect(stdout.mock.calls[0][0]).toMatch(/a: C/);
    expect(stdout.mock.calls[0][0]).toMatch(/l: layout2/);
  });
});
