export interface ApiActionOptionsBase {
  url: string;
  key: string;
}
