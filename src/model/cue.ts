import {Action} from './action';

export interface Cue {
  version?: string;
  startTime: number;
  endTime?: number;
  actions: Action[];
}
