export interface Action {
  component: string;
  action: string;
  payload?: any;
}
