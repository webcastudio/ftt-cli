import {head, tail} from './parse.actions';


jest.mock('fs');
const {readFileSync} = require('fs');

describe('parse actions', () => {
  let cuepoints;
  let stdout;


  beforeEach(() => {
    cuepoints = [
      {
        version: 2,
        startTime: 0,
        endTime: 10,
        actions: [
          {
            component: 'container',
            action: 'default'
          },
          {
            component: 'slideshows',
            action: 'default'
          }
        ]
      },
      {
        version: 2,
        startTime: 10,
        endTime: 20,
        actions: [
          {
            component: 'container',
            action: 'changeLayout',
            payload: {
              layoutClass: 'layout2'
            }
          },
          {
            component: 'slideshows',
            action: 'default'
          }
        ]
      },
      {
        version: 2,
        startTime: 10,
        endTime: 20,
        actions: [
          {
            component: 'container',
            action: 'refreshLayout',
            payload: {
              layoutClass: 'layout2'
            }
          },
          {
            component: 'slideshows',
            action: 'changeSlide',
            payload: {
              contentId: 1,
              slide: 1,
              step: 0
            }
          }
        ]
      },
      {
        version: 2,
        startTime: 20,
        endTime: 30,
        actions: [
          {
            component: 'container',
            action: 'refreshLayout',
            payload: {
              layoutClass: 'layout2'
            }
          },
          {
            component: 'slideshows',
            action: 'refreshSlide',
            payload: {
              contentId: 1,
              slide: 1,
              step: 0
            }
          }
        ]
      }
    ];
    readFileSync
      .mockReturnValue(Buffer.from(JSON.stringify(cuepoints)));
  });

  beforeEach(() => {
    stdout = jest.spyOn(console, 'log');
  });

  afterEach(() => {
    stdout.mockRestore();
  });

  describe('head', () => {
    it('should show the 2 first cues', () => {
      head('cuepoints.json', {
        number: 2
      });
      expect(stdout.mock.calls[0][0]).toMatch(/a: D/);
      expect(stdout.mock.calls[0][0]).toMatch(/a: C/);
      expect(stdout.mock.calls[0][0]).not.toMatch(/a: R/);
      expect(stdout.mock.calls[0][0]).toMatch(/l: layout2/);
    });
  });

  describe('tail', () => {
    it('should show the 2 last cues', () => {
      tail('cuepoints.json', {
        number: 2
      });
      expect(stdout.mock.calls[0][0]).not.toMatch(/a: D/);
      expect(stdout.mock.calls[0][0]).toMatch(/a: C/);
      expect(stdout.mock.calls[0][0]).toMatch(/a: R/);
      expect(stdout.mock.calls[0][0]).toMatch(/l: layout2/);
      expect(stdout.mock.calls[0][0]).toMatch(/cId: 1/);
      expect(stdout.mock.calls[0][0]).toMatch(/slide: 1/);
      expect(stdout.mock.calls[0][0]).toMatch(/step: 0/);
    });
  });

  it('should print only specified components', () => {
    head('cuepoints.json', {
      number: 2,
      components: ['container']
    });

    expect(stdout.mock.calls[0][0]).not.toMatch(/slideshows/);
  });

});
