import {readFileSync} from 'fs';
import {ApiActionOptionsBase} from './model/api-action-options-base';
import {ApiClient, WebcastEventsApi} from '@webcastudio/sdk';
import {basename} from 'path';

export interface PutActionOptions extends ApiActionOptionsBase {
  eventId: number;
  sessionId: number;
  languageId: number;

}
export async function put(file: string, cmd: PutActionOptions) {
  if (!cmd.eventId
    || !cmd.sessionId
    || !cmd.languageId) {
    throw new Error('Invalid arguments');
  }
  const buff = readFileSync(file);

  ApiClient.configure({
    url: cmd.url,
    credentials: {
      apiKey: cmd.key
    }
  });
  const events = new WebcastEventsApi();

  const where: any = {
    sessionId: cmd.sessionId,
    languageId: cmd.languageId
  };

  const ods = await events.findOnDemands(cmd.eventId, {
    where: JSON.stringify(where)
  });

  if (ods.totalItems === 0) {
    throw new Error('No OnDemand has been created for this session.');
  }

  (<any>buff).path = basename(file);

  await events.uploadOndemandCuepoints(cmd.eventId, ods.items[0].id, buff);
}
