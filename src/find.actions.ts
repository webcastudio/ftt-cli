import {read} from './util/read';
import {Cue} from './model/cue';
import {print} from './util/print';
import {str2time} from './util/time';

export interface FindActionOptions {
  time?: number | string;
  components?: string[];
}
export function find(file, cmd: FindActionOptions) {
  let data: Cue[];

  try {
    data = read(file);
  } catch (e) {
    console.error('The file provided is not a valid JSON file.');
    process.exit(1);
    return;
  }

  if (cmd.time != null) {
    let time = cmd.time;

    if (typeof time === 'string') {
      time = str2time(time);
    }
    const cues = data.filter((c) => {
      return time >= c.startTime && time <= c.endTime;
    });

    print(cues, cmd.components);
    return;
  }
}
